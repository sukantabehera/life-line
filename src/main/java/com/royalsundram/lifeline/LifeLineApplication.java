package com.royalsundram.lifeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeLineApplication {

	public static void main(String[] args) {
		SpringApplication.run(LifeLineApplication.class, args);
	}

}
